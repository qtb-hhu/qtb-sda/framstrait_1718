### script to plot top abundance taxa per cluster

library(ggplot2)
library(ggpubr)
library(ggridges)
library(patchwork)
library(reshape2)
library(RColorBrewer)
library(tibble)
source("calcTimeSeriesInfo.R")

load("data/clusters/HG-IV-TimeSeriesData-euk.Rdata")

## number of cluster for plot

nclu=10
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==10)]="H-08"
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==1)]="H-06"
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==2)]="H-02"
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==3)]="H-09"
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==4)]="H-10"
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==5)]="H-04"
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==6)]="H-01"
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==7)]="H-07"
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==8)]="H-05"
TimeSeriesData$pts_data$cset$clusters[which(TimeSeriesData$pts_data$cset$clusters==9)]="H-03"


clusterVector=TimeSeriesData$pts_data$cset$clusters[,paste0("K:",nclu)]

clusAVG=aggregate(TimeSeriesData$abundMatNorm,list(clusterVector),"mean")
rownames(clusAVG)=c("H-06","H-02","H-09","H-10","H-04","H-01","H-07","H-05","H-03","H-08")
clusAVG=clusAVG[,2:ncol(clusAVG)]#2:nrow(clusAVG)
#phase shifts
pts=apply(clusAVG,1,calcTimeSeriesInfo)
allSins=sapply(pts,function(x) x$modData_sin)
ppf=sapply(pts,function(x)x$phase_freq,simplify = T)
#clusterCol
clusterCol=TimeSeriesData$pts_data$cset$colors[[paste0("K:",nclu)]]


ddf=data.frame(date=rep(TimeSeriesData$date_vec,ncol(allSins)),
               sin=melt(allSins)[,c(2,3)],
               #avg=melt(t(TimeSeriesData$cluster_average))[,3])
               avg=melt(t(clusAVG))[,3])
colnames(ddf)=c("date","module","sinus","avg")
# here it is wrong NAs
#ddf$module=factor(ddf$module,levels=as.character(order(ppf[1,],decreasing = F)),ordered=T)
modSize=table(clusterVector)


tax_level="Class"
taxa=TimeSeriesData$taxa_info[,tax_level]
# abundance per cluster and taxa
abundMat=list()

for(i in clusterVector){
  mi=which(clusterVector==i)
  # average per year and sum per taxa
  abundMat[[i]]=rowsum(rowMeans(TimeSeriesData$abundMatNorm[mi,]),taxa[mi])
  tax=rownames(abundMat[[i]])
  new_tax=rep("Others",length(tax))
  #order by size decreasing
  oi=order(abundMat[[i]],decreasing = T)
  # define the how many top species should be considered
  ntop=10
  new_tax[head(oi,ntop)]=tax[head(oi,ntop)]
  #abundMat[[i]]=abundMat[[i]][oi,]
  #new_tax=new_tax[oi]
  # add cluster number and taxa name
  abundMat[[i]]=cbind(abundMat[[i]],rep(i,length(abundMat[[i]])),new_tax)
  colnames(abundMat[[i]])=c("abundance","module","taxa")
}
#combine all modules
TaxAbund=as.data.frame(do.call("rbind",abundMat))
# order by phase
#TaxAbund$module=factor(TaxAbund$module,levels=as.character(order(TaxAbund$module,decreasing = F)),ordered=T)
TaxAbund$abundance=as.numeric(TaxAbund$abundance)
TaxAbund$taxa=factor(TaxAbund$taxa)
TaxAbund$taxa=reorder(TaxAbund$taxa,TaxAbund$abundance)
#ii=which(levels(TaxAbund$taxa)!="Others")
#levels(TaxAbund$taxa)=c("Others",levels(TaxAbund$taxa)[ii])

#cc=colorRampPalette(c(brewer.pal(8,"Dark2"),rainbow(10),brewer.pal(12,"Paired"),okabe))
cc=colorRampPalette(rev(brewer.pal(12,"Paired")))
#taxCol=cc(length(unique(TaxAbund$taxa))-8)
taxCol=cc(length(unique(TaxAbund$taxa))-(length(unique(TaxAbund$taxa))-10))
#taxCol=c("white",taxCol,rev(brewer.pal(8,"Set1")))
taxCol=c(rep("white",length(unique(TaxAbund$taxa))-10),taxCol,rev(brewer.pal(8,"Set1")))

### absolut abundace taxa ####
TaxAbund$module=factor(TaxAbund$module, levels=sort(unique(TaxAbund$module),decreasing = T))
p3=
  ggplot(TaxAbund,aes(x=module,abundance,fill=taxa))+
  geom_bar(stat="identity",width=0.9,show.legend = T,color="black",size=0.1)+
  coord_flip()+
  scale_fill_manual(values=taxCol)+
  theme(legend.key.size = unit(1, 'cm'))+
  theme_minimal()+
  guides(fill=guide_legend(ncol=1,title = "Taxa"))+
  theme(legend.key.size = unit(0.35, 'cm'))
#p3

## get proportional abundance ####
p4=ggplot(TaxAbund,aes(x=module,y=abundance,fill=taxa))+
  geom_bar(position="fill",stat="identity",width=0.9,colour="black",size=0.1)+
  coord_flip()+
  scale_fill_manual(values=taxCol)+
  theme_minimal()+
  guides(fill=guide_legend(ncol=1,title = "Taxa"))+
  theme(legend.key.size = unit(0.35, 'cm'))
  
#p4

### print figures ####
plot1=wrap_plots(p3,p4,widths = c(2,1))
plot1





