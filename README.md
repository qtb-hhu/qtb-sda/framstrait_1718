## Name
Framstrait 1718

## Description
Script for reproducing the tables and figures of the manuscript.

## Data
Raw and processed mooring data are available at PANGAEA <br>
https://doi.org/10.1594/PANGAEA.904565, <br>
https://doi.org/10.1594/PANGAEA.940744, <br>
https://doi.pangaea.de/10.1594/PANGAEA.941125. 

## Contributing
Ellen Oldenburg

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
Ellen Oldenburg, Ovidiu Popa, Matthias Wietz, Wilken-Jon von Appen, Sinhue Torres-Valdes, Christina Bienhold, Oliver Ebenhöh and Katja Metfies

## Citation
By using this code cite:
```
@article{EO2023,
  title={Sea-ice melt determines seasonal phytoplankton dynamics and delimits the habitat of temperate Atlantic taxa as the Arctic Ocean atlantifies},
  author={Ellen Oldenburg, Ovidiu Popa, Matthias Wietz, Wilken-Jon von Appen,
   Sinhue Torres-Valdes, Christina Bienhold, Oliver Ebenhöh and Katja Metfies
},
  GitHub={git@gitlab.com:qtb-hhu/qtb-sda/framstrait_1718.git},
  year={2023}
}
```

## License
MIT

## Project status
In Submission
